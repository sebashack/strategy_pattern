use serde_json::Value;
use std::path::PathBuf;

use strategy_pattern::public_transport_algorithm::PublicTransportSolver;
use strategy_pattern::road_algorithm::RoadSolver;
use strategy_pattern::route_solver::RouteSolver;
use strategy_pattern::walking_algorithm::WalkingSolver;

type VecSol = Vec<(f32, f32)>;

fn solve_from_string(input: String, sl: &impl RouteSolver<String, Solution = VecSol>) -> VecSol {
    sl.get_coordinates(input)
}

fn solve_from_file(input: PathBuf, sl: &impl RouteSolver<PathBuf, Solution = VecSol>) -> VecSol {
    sl.get_coordinates(input)
}

fn solve_from_json(input: Value, sl: &impl RouteSolver<Value, Solution = Value>) -> Value {
    sl.get_coordinates(input)
}

fn main() {
    println!("###### RoadSolver ###### ");
    let road_solver = RoadSolver::new();

    solve_from_string(String::from("input"), &road_solver);
    solve_from_file(PathBuf::from("some/path/to/input"), &road_solver);
    solve_from_json(serde_json::to_value("input").unwrap(), &road_solver);

    println!("###### PublicTransportSolver ###### ");
    let ptransport_solver = PublicTransportSolver::new();

    solve_from_string(String::from("input"), &ptransport_solver);
    solve_from_file(PathBuf::from("some/path/to/input"), &ptransport_solver);
    solve_from_json(serde_json::to_value("input").unwrap(), &ptransport_solver);

    println!("###### WalkingSolver ###### ");
    let walking_solver = WalkingSolver::new();

    solve_from_string(String::from("input"), &walking_solver);
    solve_from_file(PathBuf::from("some/path/to/input"), &walking_solver);
    solve_from_json(serde_json::to_value("input").unwrap(), &walking_solver);
}
