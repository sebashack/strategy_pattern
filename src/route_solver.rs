pub trait RouteSolver<Input> {
    type Solution;
    fn get_coordinates(&self, input: Input) -> Self::Solution;
}
