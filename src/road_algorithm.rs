use serde_json::Value;
use std::path::PathBuf;

use super::route_solver::RouteSolver;

pub struct RoadSolver {}

impl RoadSolver {
    pub fn new() -> Self {
        RoadSolver {}
    }
}

impl RouteSolver<String> for RoadSolver {
    type Solution = Vec<(f32, f32)>;

    fn get_coordinates(&self, input: String) -> Self::Solution {
        println!("[RoadSolver] processing string {} ...", input);

        vec![(2.0, 3.1), (3.1, 2.4), (5.0, 2.3)]
    }
}

impl RouteSolver<PathBuf> for RoadSolver {
    type Solution = Vec<(f32, f32)>;

    fn get_coordinates(&self, input: PathBuf) -> Self::Solution {
        println!(
            "[RoadSolver] processing path {} ...",
            input.as_os_str().to_str().unwrap()
        );

        vec![(2.0, 3.1), (3.1, 2.4), (5.0, 2.3)]
    }
}

impl RouteSolver<Value> for RoadSolver {
    type Solution = Value;

    fn get_coordinates(&self, input: Value) -> Self::Solution {
        println!("[RoadSolver] processing json {}", input.to_string());

        serde_json::to_value(vec![(2.0, 3.1), (3.1, 2.4), (5.0, 2.3)]).unwrap()
    }
}
