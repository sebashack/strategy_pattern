use serde_json::Value;
use std::path::PathBuf;

use super::route_solver::RouteSolver;

pub struct PublicTransportSolver {}

impl PublicTransportSolver {
    pub fn new() -> Self {
        PublicTransportSolver {}
    }
}

impl RouteSolver<String> for PublicTransportSolver {
    type Solution = Vec<(f32, f32)>;

    fn get_coordinates(&self, input: String) -> Self::Solution {
        println!("[PublicTransportSolver] processing string {} ...", input);

        vec![(4.0, 2.9), (3.6, 7.9), (6.8, 8.7)]
    }
}

impl RouteSolver<PathBuf> for PublicTransportSolver {
    type Solution = Vec<(f32, f32)>;

    fn get_coordinates(&self, input: PathBuf) -> Self::Solution {
        println!(
            "[PublicTransportSolver] processing path {} ...",
            input.as_os_str().to_str().unwrap()
        );

        vec![(4.0, 2.9), (3.6, 7.9), (6.8, 8.7)]
    }
}

impl RouteSolver<Value> for PublicTransportSolver {
    type Solution = Value;

    fn get_coordinates(&self, input: Value) -> Self::Solution {
        println!(
            "[PublicTransportSolver] processing json {}",
            input.to_string()
        );

        serde_json::to_value(vec![(4.0, 2.9), (3.6, 7.9), (6.8, 8.7)]).unwrap()
    }
}
