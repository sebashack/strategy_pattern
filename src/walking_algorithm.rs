use serde_json::Value;
use std::path::PathBuf;

use super::route_solver::RouteSolver;

pub struct WalkingSolver {}

impl WalkingSolver {
    pub fn new() -> Self {
        WalkingSolver {}
    }
}

impl RouteSolver<String> for WalkingSolver {
    type Solution = Vec<(f32, f32)>;

    fn get_coordinates(&self, input: String) -> Self::Solution {
        println!("[WalkingSolver] processing string {} ...", input);

        vec![(5.1, 6.9), (4.5, 3.3), (9.8, 8.8)]
    }
}

impl RouteSolver<PathBuf> for WalkingSolver {
    type Solution = Vec<(f32, f32)>;

    fn get_coordinates(&self, input: PathBuf) -> Self::Solution {
        println!(
            "[WalkingSolver] processing path {} ...",
            input.as_os_str().to_str().unwrap()
        );

        vec![(5.1, 6.9), (4.5, 3.3), (9.8, 8.8)]
    }
}

impl RouteSolver<Value> for WalkingSolver {
    type Solution = Value;

    fn get_coordinates(&self, input: Value) -> Self::Solution {
        println!("[WalkingSolver] processing json {}", input.to_string());

        serde_json::to_value(vec![(5.1, 6.9), (4.5, 3.3), (9.8, 8.8)]).unwrap()
    }
}
